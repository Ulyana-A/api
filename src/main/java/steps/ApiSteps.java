package steps;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ApiSteps {

    @Step("Получить список пользоваталей")
    public void getUserList(){

        Response response = (Response) RestAssured.given()
                .baseUri("https://reqres.in")
                .when()
                .get("/api/users?page")
                .then()
                .assertThat()
                .statusCode(200).extract().response().body();
        System.out.println();
    }


}
